const express = require('express');
const port = process.env.PORT || 5000;


const app = express();


app.use(express.json());
app.use(express.urlencoded({extended: true}))
app.use(express.static('public'));

app.get('/form', (req, res) => {
    res.sendFile(__dirname + '/public/test');
})
app.post('/formPost', (req, res) => {

    console.log(req.body);
})

app.listen(port, () => {

    console.log(`Server started at http://localhost:${port}`)
});
